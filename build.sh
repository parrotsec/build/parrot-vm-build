#!/bin/bash
# Official VM Builder
# This script is designed to build virtual machine images for ParrotOS.
# It includes customizable parameters for version, codename, editions, architectures, image formats, size, and compression.

# Define color codes for terminal output to enhance readability:
RED='\033[0;31m'
GREEN='\033[0;32m'
BLUE='\033[0;34m'
YELLOW='\033[0;33m'
NC='\033[0m' # No Color: resets to the terminal's default color

# Default configuration parameters:
VERSION="6.3"
CODENAME="lory"
DEFAULT_EDITIONS=("home" "security")
DEFAULT_ARCHS=("amd64" "arm64")
IMAGE_FORMATS=("qcow2" "vmdk" "vdi" "ova") # List of supported image formats
DEFAULT_IMAGE_FORMATS=("qcow2")
DEFAULT_IMAGE_SIZE="64G" # Default image size; can be adjusted as needed (e.g. 32G, 64G, 128G, etc...)
COMPRESS=true
OUTPUT_DIR="images" # Default directory where the created images will be stored

# This helps users understand how to properly use the script and what each option does
function show_usage {
    echo -e "${BLUE}Parrot VM Builder${NC}"
    echo "Usage: $0 [OPTIONS]"
    echo ""
    echo "Options:"
    echo "  -h, --help                Show this help message"
    echo "  -v, --version VERSION     Specify the version (default: $VERSION)"
    echo "  -c, --codename CODENAME   Specify the codename (default: $CODENAME)"
    echo "  -e, --editions EDITIONS   Specify the editions to create (default: ${DEFAULT_EDITIONS[*]})"
    echo "                            Possible values: home, security"
    echo "  -a, --archs ARCHS         Specify the architectures to create (default: ${DEFAULT_ARCHS[*]})"
    echo "                            Possible values: amd64, arm64"
    echo "  -f, --formats FORMATS     Specify the image formats to create (default: ${DEFAULT_IMAGE_FORMATS[*]})"
    echo "                            Possible values: ${IMAGE_FORMATS[*]}"
    echo "  -s, --size SIZE           Specify the image size (default: $DEFAULT_IMAGE_SIZE)"
    echo "  -o, --output DIR          Specify the output directory (default: $OUTPUT_DIR)"
    echo "  -n, --no-compress         Do not compress the final images"
    echo ""
    echo "Examples:"
    echo "  $0 --version 6.0 --editions home --archs amd64 --formats qcow2,vmdk"
    echo "  $0 -v 6.0 -e security -a arm64 -f ova -s 32G"
    echo ""
    exit 0  # Exit after showing usage; this is standard behavior for a help message
}


function log {
    local level=$1
    local message=$2
    local color=$NC
    
    case $level in
        "INFO") color=$BLUE ;;
        "SUCCESS") color=$GREEN ;;
        "WARNING") color=$YELLOW ;;
        "ERROR") color=$RED ;;
    esac
    
    echo -e "${color}[$level]${NC} $message"
}

function error_exit {
    log "ERROR" "$1"
    exit 1 
}

# If the command is not found, the script exits with an error message
function check_command {
    command -v "$1" >/dev/null 2>&1 || error_exit "$1 is not installed. Install it using 'apt install $1'"
}

# This is particularly useful in a chroot environment where certain system directories need to be available
function _mount {
    log "INFO" "Mounting directories for $1"
    
    mkdir -p "$1/proc" "$1/sys" "$1/dev" "$1/run" "$1/tmp" "$1/var/tmp"
    
    # Mount various pseudo-filesystems to provide required system information
    mount --make-slave -t proc proc "$1/proc/" || error_exit "Unable to mount proc"
    mount --make-slave -t sysfs sysfs "$1/sys/" || error_exit "Unable to mount sysfs"
    mount --make-slave -t devtmpfs dev "$1/dev/" || error_exit "Unable to mount devtmpfs"
    
    # Ensure that the 'pts' directory exists before mounting devpts
    mkdir -p "$1/dev/pts/"
    mount --make-slave -t devpts devpts "$1/dev/pts/" || error_exit "Unable to mount devpts"
    
    # Mount temporary file systems for runtime directories
    mount --make-slave -t tmpfs tmpfs "$1/run/" || error_exit "Unable to mount tmpfs (run)"
    mount --make-slave -t tmpfs tmpfs "$1/tmp/" || error_exit "Unable to mount tmpfs (tmp)"
    mount --make-slave -t tmpfs tmpfs "$1/var/tmp/" || error_exit "Unable to mount tmpfs (var/tmp)"
    
    log "SUCCESS" "Directories mounted successfully"
}

function _umount {
    log "INFO" "Unmounting directories for $1"
    
    # Use lazy unmount (-l) to unmount directories, which is less aggressive than forcing a detach,
    # reducing the risk of errors if some resources are still in use
    umount -l "$1/dev/pts" 2>/dev/null || true
    umount -l "$1/dev" 2>/dev/null || true
    umount -l "$1/proc/sys/fs/binfmt_misc" 2>/dev/null || true
    umount -l "$1/proc" 2>/dev/null || true
    umount -l "$1/sys" 2>/dev/null || true
    umount -l "$1/run" 2>/dev/null || true
    umount -l "$1/tmp" 2>/dev/null || true
    umount -l "$1/var/tmp" 2>/dev/null || true
    
    # This uses lsof to list open files; if there are any, it warns and waits
    if lsof +D "$1" 2>/dev/null; then
        log "WARNING" "There are still active processes in the chroot. Waiting 5 seconds..."
        sleep 5
        
        # Forcefully kill any remaining processes using the target directory
        fuser -k -9 "$1" 2>/dev/null || true
    fi
    
    log "SUCCESS" "Directories unmounted successfully"
}

# Initialize the base system using debootstrap
function bootstrap {
    local ARCH=$1
    local EDITION=$2
    local CODENAME=$3
    local TARGET_DIR="${EDITION}-${ARCH}"
    
    log "INFO" "Starting bootstrap for ${EDITION}-${ARCH}"
    
    # If the target directory already exists, remove it to start fresh
    if [ -d "$TARGET_DIR" ]; then
        log "WARNING" "The directory $TARGET_DIR already exists. Removing it..."
        rm -rf "$TARGET_DIR" || error_exit "Unable to remove the directory $TARGET_DIR"
    fi
    
    log "INFO" "Creating the base system with debootstrap..."
    debootstrap --arch="$ARCH" --components=main,contrib,non-free,non-free-firmware \
                --include=gnupg2,nano,base-files,qemu-user-static \
                --exclude=parrot-core,debian-archive-keyring \
                "$CODENAME" "$TARGET_DIR" https://deb.parrot.sh/direct/parrot/ || \
                error_exit "Error during debootstrap execution"
    
    log "INFO" "Customizing the base system..."
    _mount "$TARGET_DIR"
    chroot "$TARGET_DIR" bash -c "apt update && echo | apt -y install parrot-core" || \
        error_exit "Error during the installation of parrot-core package"
    
    mkdir -p "$TARGET_DIR/var/tmp" "$TARGET_DIR/tmp"
    _umount "$TARGET_DIR"
    
    # Clean the APT cache to reduce the final image size
    rm -rf "$TARGET_DIR/var/cache/apt/"* "$TARGET_DIR/var/lib/apt/lists/"*
    
    log "SUCCESS" "Bootstrap completed for ${EDITION}-${ARCH}"
}

function configure_home {
    local ARCH=$1
    local EDITION=$2
    local TARGET_DIR="${EDITION}-${ARCH}"
    
    log "INFO" "Configuring Home Edition for ${ARCH}"
    
    _mount "$TARGET_DIR"
    
    # Install X11 packages to support the graphical interface
    log "INFO" "Installing X11 packages..."
    chroot "$TARGET_DIR" bash -c "DEBIAN_FRONTEND=noninteractive apt update && apt -y install xserver-xorg-input-all xserver-xorg-video-all" || \
        log "WARNING" "Error during the installation of X11 packages"
    
    # Install the kernel and GRUB bootloader
    log "INFO" "Installing the kernel and GRUB..."
    chroot "$TARGET_DIR" bash -c "DEBIAN_FRONTEND=noninteractive apt -y install linux-image-$ARCH linux-headers-$ARCH grub-efi-$ARCH" || \
        log "WARNING" "Error during the kernel installation"
    
    # Install essential system packages, including locales, boot themes...
    log "INFO" "Installing system packages..."
    chroot "$TARGET_DIR" bash -c "echo | DEBIAN_FRONTEND=noninteractive apt -y install locales locales-all plymouth plymouth-themes tasksel cryptsetup cryptsetup-nuke-password iw lvm2 mdadm parted gpart bash-completion rng-tools5 haveged" || \
        log "WARNING" "Error during the installation of system packages"
    
    # Install additional system utilities
    log "INFO" "Installing system utilities..."
    chroot "$TARGET_DIR" bash -c "DEBIAN_FRONTEND=noninteractive apt -y install neovim inxi neofetch htop nload iftop jfsutils hfsplus hfsutils btrfs-progs e2fsprogs dosfstools mtools reiser4progs reiserfsprogs xfsprogs xfsdump ntfs-3g libfsapfs1 libfsapfs-utils apparmor apparmor-profiles apparmor-profiles-extra apparmor-utils apparmor-easyprof apparmor-notify" || \
        log "WARNING" "Error during the installation of system utilities"
    
    # Install drivers and tools optimized for virtualized environments
    log "INFO" "Installing drivers for virtualized environments..."
    chroot "$TARGET_DIR" bash -c "DEBIAN_FRONTEND=noninteractive apt -y install qemu-guest-agent spice-vdagent open-vm-tools-desktop" || \
        log "WARNING" "Error during the installation of virtualization drivers"
    
    # Install packages from backports to get newer or additional features
    log "INFO" "Installing packages from backports..."
    chroot "$TARGET_DIR" bash -c "apt -y -t lory-backports install virtualbox-guest-x11 parrot-meta-privacy" || \
        log "WARNING" "Error during the installation of backports packages (non-critical)"
    
    # Install the DE along with development tools
    log "INFO" "Installing desktop environment and development tools..."
    chroot "$TARGET_DIR" bash -c "DEBIAN_FRONTEND=noninteractive apt -y -t lory-backports install parrot-meta-devel parrot-meta-crypto parrot-interface parrot-interface-home parrot-desktop-mate parrot-meta-devel codium golang-go nodejs npm default-jdk python3-pip git podman podman-docker flatpak flatpak-xdg-utils firefox-esr chromium- mate-user-guide- pocketsphinx-en-us- libreoffice-help-en-us- mythes-en-us- libreoffice-help-common- espeak-ng-data-" || \
        log "WARNING" "Error during the installation of the desktop environment"
    
    # Update the menu launchers to ensure all shortcuts are properly configured
    log "INFO" "Updating menu launchers..."
    chroot "$TARGET_DIR" bash -c "/usr/share/parrot-menu/update-launchers" || \
        log "WARNING" "Error updating menu launchers"
    
    # Configure the login greeter for the display manager
    log "INFO" "Configuring lightdm..."
    chroot "$TARGET_DIR" bash -c "cp /etc/lightdm/slick-greeter-$EDITION.conf /etc/lightdm/slick-greeter.conf || true"
    
    log "INFO" "Final package upgrade..."
    chroot "$TARGET_DIR" bash -c "DEBIAN_FRONTEND=noninteractive apt update && apt -y full-upgrade -t lory-backports" || \
        log "WARNING" "Error during the package update"
    
    # IMPORTANT: Clean up the system by removing unnecessary packages and cleaning the cache
    log "INFO" "Cleaning the system..."
    chroot "$TARGET_DIR" bash -c "DEBIAN_FRONTEND=noninteractive apt -y autoremove --purge && apt clean" || \
        log "WARNING" "Error during system cleanup"
   
    log "INFO" "Disabling unnecessary services..."
    chroot "$TARGET_DIR" bash -c "systemctl disable postgres || true; systemctl disable gsad || true; systemctl disable redis-server || true; systemctl disable xrdp || true; systemctl disable opensnitch || true; systemctl disable ssh || true"
    
    log "INFO" "Updating initramfs..."
    chroot "$TARGET_DIR" bash -c "update-initramfs -u" || \
        log "WARNING" "Error updating initramfs"
    
    # Create a new user with a predefined password "parrot"
    log "INFO" "Creating user..."
    chroot "$TARGET_DIR" bash -c "useradd -m -s /bin/bash -p \"\$(openssl passwd parrot)\" user" || \
        log "WARNING" "Error creating user"
    
    echo -e "Username: user\nPassword: parrot\n\nChange password using the passwd command" > "$TARGET_DIR/home/user/Desktop/password.txt"
    
    # Configure the /etc/hosts file with basic host entries
    echo -e "# Host addresses\n127.0.0.1  localhost\n127.0.1.1  parrot\n::1        localhost ip6-localhost ip6-loopback\nff02::1    ip6-allnodes\nff02::2    ip6-allrouters\n# Others" > "$TARGET_DIR/etc/hosts"
    
    # Set the system hostname
    echo -e "parrot" > "$TARGET_DIR/etc/hostname"
    
    # Add the user to various system groups for necessary permissions
    log "INFO" "Adding user to appropriate groups..."
    chroot "$TARGET_DIR" bash -c 'for group in dialout cdrom floppy sudo audio dip video plugdev netdev bluetooth lpadmin scanner docker; do adduser user $group || true; done'
    
    log "INFO" "Configuring autologin..."
    chroot "$TARGET_DIR" bash -c "sed -i \"s/#autologin-user=/autologin-user=user/\" /etc/lightdm/lightdm.conf"
    
    log "INFO" "Configuring passwordless sudo..."
    chroot "$TARGET_DIR" bash -c "echo \"user ALL=(ALL) NOPASSWD: ALL\" > /etc/sudoers.d/20-parrot-virtual"
    
    # Remove Python cache directories and bash history to clean up the system
    find "$TARGET_DIR" -type d -name "__pycache__" -delete || true
    rm "$TARGET_DIR/root/.bash_history" || true
    
    _umount "$TARGET_DIR"
    
    log "SUCCESS" "Home Edition configuration completed!"
}

# Configure the Security Edition by copying the base Home Edition and adding security-specific tools:
function configure_security {
    local ARCH=$1
    local EDITION=$2
    local HOME_DIR="home-$ARCH" # Directory name for the Home Edition for the given architecture
    local TARGET_DIR="${EDITION}-${ARCH}"
    
    log "INFO" "Configuring Security Edition for $ARCH"
    
    # Copy files from the Home Edition to create a base for the Security Edition
    # This ensures that the Security Edition inherits the base configuration from Home Edition
    if [ -d "$HOME_DIR" ]; then
        log "INFO" "Copying files from the Home Edition..."
        cp -rp --reflink "$HOME_DIR" "$TARGET_DIR" || \
            error_exit "Unable to copy files from the Home Edition"
    else
        error_exit "Home Edition directory ($HOME_DIR) not found"
    fi
    
    _mount "$TARGET_DIR"
    
    # Install security tools specific to the Security Edition
    log "INFO" "Installing security tools..."
    chroot "$TARGET_DIR" bash -c "DEBIAN_FRONTEND=noninteractive apt -y install parrot-tools-full" || \
        log "WARNING" "Error during the installation of security tools"
    
    log "INFO" "Configuring the login manager..."
    chroot "$TARGET_DIR" bash -c "cp /etc/lightdm/slick-greeter-security.conf /etc/lightdm/slick-greeter.conf || true"
    
    log "INFO" "Cleaning up the system..."
    chroot "$TARGET_DIR" bash -c "DEBIAN_FRONTEND=noninteractive apt -y autoremove --purge && apt clean" || \
        log "WARNING" "Error during system cleanup"
    
    # The following lines are commented out by default to disable Flatpak Postman installation.
    # They can be uncommented if Flatpak application installation is desired.
    #log "INFO" "Installing Flatpak applications..."
    #chroot "$TARGET_DIR" bash -c "flatpak -y install flathub com.getpostman.Postman" || \
    #    log "WARNING" "Error during the installation of Flatpak applications"
    
    log "INFO" "Disabling unnecessary services..."
    chroot "$TARGET_DIR" bash -c "systemctl disable postgres || true; systemctl disable gsad || true; systemctl disable redis-server || true; systemctl disable xrdp || true; systemctl disable opensnitch || true; systemctl disable ssh || true"
    
    find "$TARGET_DIR" -type d -name "__pycache__" -delete || true
    rm "$TARGET_DIR/root/.bash_history" || true
    
    _umount "$TARGET_DIR"
    
    log "SUCCESS" "Security Edition configuration completed!"
}

function create_image {
    local ARCH=$1
    local EDITION=$2
    local VERSION=$3
    local FORMAT=$4
    local SIZE=$5
    local TARGET_DIR="${EDITION}-${ARCH}"
    local OUTPUT_FILE="$OUTPUT_DIR/Parrot-$EDITION-${VERSION}_$ARCH"
    local TEMP=$(mktemp -d)
    
    log "INFO" "Creating $FORMAT image for ${EDITION}-${ARCH}"
    
    if [ ! -d "$TARGET_DIR" ]; then
        error_exit "Directory $TARGET_DIR not found"
    fi
    
    mkdir -p "$OUTPUT_DIR"
    
    case $FORMAT in
        "qcow2")
            log "INFO" "Creating QEMU qcow2 image..."
            qemu-img create -f qcow2 "$OUTPUT_FILE.qcow2" "$SIZE" || \
                error_exit "Unable to create qcow2 image"
            
            # Load the NBD module if not already loaded
            if ! lsmod | grep -q nbd; then
                log "INFO" "Loading NBD module..."
                modprobe nbd max_part=8 || error_exit "Unable to load NBD module"
                sleep 2
            fi
            
            # Disconnect any existing attachment and then attach the new qcow2 image to /dev/nbd0
            log "INFO" "Attaching image to /dev/nbd0..."
            qemu-nbd --disconnect /dev/nbd0 >/dev/null 2>&1 || true
            sleep 1
            qemu-nbd --connect /dev/nbd0 "$OUTPUT_FILE.qcow2" || \
                error_exit "Unable to attach the image to /dev/nbd0"
            sleep 2
            
            # First partition of +512M for EFI, second partition takes the rest
            log "INFO" "Creating partitions..."
            echo -e "g\nn\n1\n\n+512M\nt\n1\nn\n2\n\n\np\nw" | fdisk /dev/nbd0 || \
                error_exit "Unable to create partitions"
            sleep 1
            
            log "INFO" "Creating filesystems..."
            mkfs.fat -F 32 /dev/nbd0p1 || error_exit "Unable to create FAT32 filesystem"
            mkfs.btrfs -f /dev/nbd0p2 || error_exit "Unable to create Btrfs filesystem"
            
            log "INFO" "Creating Btrfs subvolumes..."
            mount /dev/nbd0p2 "$TEMP" || error_exit "Unable to mount /dev/nbd0p2"
            btrfs subvol create "$TEMP/@" || error_exit "Unable to create subvolume @"
            btrfs subvol create "$TEMP/@home" || error_exit "Unable to create subvolume @home"
            umount "$TEMP"
            sleep 2
            
            log "INFO" "Mounting Btrfs subvolumes..."
            mount -o subvol=@,autodefrag,compress=zstd:15 /dev/nbd0p2 "$TEMP" || \
                error_exit "Unable to mount subvolume @"
            mkdir -p "$TEMP/home"
            mount -o subvol=@home,autodefrag,compress=zstd:15 /dev/nbd0p2 "$TEMP/home" || \
                error_exit "Unable to mount subvolume @home"
            mkdir -p "$TEMP/boot/efi"
            mount /dev/nbd0p1 "$TEMP/boot/efi" || error_exit "Unable to mount /dev/nbd0p1"
            sleep 2
            
            # Copy the built system files into the image while excluding volatile directories
            log "INFO" "Copying files to the system..."
            rsync -Pah --exclude=dev --exclude=proc --exclude=sys --exclude=run "$TARGET_DIR/" "$TEMP/" || \
                error_exit "Unable to copy files to the system"
            
            _mount "$TEMP"
            
            log "INFO" "Creating /etc/fstab..."
            echo "$(blkid | grep nbd0p1 | cut -d' ' -f2) /boot/efi vfat defaults 0 2" > "$TEMP/etc/fstab"
            echo "$(blkid | grep nbd0p2 | cut -d' ' -f2) / btrfs subvol=@,autodefrag,compress=zstd:10 0 1" >> "$TEMP/etc/fstab"
            echo "$(blkid | grep nbd0p2 | cut -d' ' -f2) /home btrfs subvol=@home,autodefrag 0 2" >> "$TEMP/etc/fstab"
            
            # IMPORTANT: Install GRUB bootloader inside the chroot environment
            log "INFO" "Installing GRUB..."
            chroot "$TEMP" bash -c "grub-install" || log "WARNING" "Error installing GRUB"
            chroot "$TEMP" bash -c "grub-install --removable" || log "WARNING" "Error installing removable GRUB"
            chroot "$TEMP" bash -c "update-initramfs -u" || log "WARNING" "Error updating initramfs"
            chroot "$TEMP" bash -c "update-grub" || log "WARNING" "Error updating GRUB"
            
            _umount "$TEMP"
            
            umount "$TEMP/boot/efi" || log "WARNING" "Error unmounting /boot/efi"
            umount "$TEMP/home" || log "WARNING" "Error unmounting /home"
            umount "$TEMP" || log "WARNING" "Error unmounting /"
            sleep 2
            
            # Disconnect the qcow2 image from /dev/nbd0
            log "INFO" "Disconnecting the image from /dev/nbd0..."
            qemu-nbd --disconnect /dev/nbd0 || log "WARNING" "Error disconnecting the image"
            sleep 2
            
            log "SUCCESS" "qcow2 image created successfully: $OUTPUT_FILE.qcow2"
            ;;
            
        "vmdk")
            if [ ! -f "$OUTPUT_FILE.qcow2" ]; then
                log "WARNING" "qcow2 file not found. Creating qcow2 image first..."
                create_image "$ARCH" "$EDITION" "$VERSION" "qcow2" "$SIZE"
            fi
            
            log "INFO" "Converting image from qcow2 to vmdk..."
            qemu-img convert -f qcow2 -O vmdk "$OUTPUT_FILE.qcow2" "$OUTPUT_FILE.vmdk" || \
                error_exit "Unable to convert image from qcow2 to vmdk"
                
            log "SUCCESS" "vmdk image created successfully: $OUTPUT_FILE.vmdk"
            ;;
            
        "vdi")
            if [ ! -f "$OUTPUT_FILE.qcow2" ]; then
                log "WARNING" "qcow2 file not found. Creating qcow2 image first..."
                create_image "$ARCH" "$EDITION" "$VERSION" "qcow2" "$SIZE"
            fi
            
            log "INFO" "Converting image from qcow2 to vdi..."
            qemu-img convert -f qcow2 -O vdi "$OUTPUT_FILE.qcow2" "$OUTPUT_FILE.vdi" || \
                error_exit "Unable to convert image from qcow2 to vdi"
                
            log "SUCCESS" "vdi image created successfully: $OUTPUT_FILE.vdi"
            ;;
            
        "ova")
            # Create the vmdk image if it does not exist already.
            if [ ! -f "$OUTPUT_FILE.vmdk" ]; then
                log "WARNING" "vmdk file not found. Creating vmdk image first..."
                create_image "$ARCH" "$EDITION" "$VERSION" "vmdk" "$SIZE"
            fi
            
            log "INFO" "Creating OVA image..."
            
            # Ensure that VBoxManage (VirtualBox) is installed as it is required for OVA export
            check_command "VBoxManage"
            
            VM_NAME="Parrot-$EDITION-${VERSION}_$ARCH"
            
            # Unregister and delete the temporary VM if it already exists
            VBoxManage unregistervm "$VM_NAME" --delete >/dev/null 2>&1 || true
            
            log "INFO" "Creating temporary virtual machine..."
            VBoxManage createvm --name "$VM_NAME" --ostype "Debian_64" --register || \
                error_exit "Unable to create the virtual machine"
            
            # Configure the virtual machine with necessary settings, do not forget to add the EFI firmware
            VBoxManage modifyvm "$VM_NAME" --memory 2048 --cpus 2 --firmware efi --acpi on --boot1 disk || \
                error_exit "Unable to configure the virtual machine"
            
            VBoxManage storagectl "$VM_NAME" --name "SATA Controller" --add sata --controller IntelAHCI || \
                error_exit "Unable to create SATA controller"
            
            VBoxManage storageattach "$VM_NAME" --storagectl "SATA Controller" --port 0 --device 0 --type hdd --medium "$OUTPUT_FILE.vmdk" || \
                error_exit "Unable to attach the disk"
            
            log "INFO" "Exporting virtual machine as OVA..."
            VBoxManage export "$VM_NAME" --output "$OUTPUT_FILE.ova" || \
                error_exit "Unable to export the virtual machine as OVA"
            
            VBoxManage unregistervm "$VM_NAME" --delete || log "WARNING" "Unable to remove temporary virtual machine"
            
            log "SUCCESS" "OVA image created successfully: $OUTPUT_FILE.ova"
            ;;
            
        *)
            error_exit "Unsupported image format: $FORMAT"
            ;;
    esac
    
    rm -rf "$TEMP"
}

# Only proceed with compression if the COMPRESS flag is set to true.
function compress_images {
    local ARCH=$1
    local EDITION=$2
    local VERSION=$3
    local OUTPUT_FILE="$OUTPUT_DIR/Parrot-$EDITION-${VERSION}_$ARCH"
    
    if [ "$COMPRESS" = true ]; then
        for FORMAT in "${IMAGE_FORMATS[@]}"; do
            if [ -f "$OUTPUT_FILE.$FORMAT" ]; then
                log "INFO" "Compressing the $FORMAT image..."
                xz -T0 -v "$OUTPUT_FILE.$FORMAT" || log "WARNING" "Error compressing $OUTPUT_FILE.$FORMAT"
                log "SUCCESS" "Image successfully compressed: $OUTPUT_FILE.$FORMAT.xz"
            fi
        done
    fi
}

function check_dependencies {
    log "INFO" "Checking dependencies..."
    
    local REQUIRED_COMMANDS=("debootstrap" "chroot" "rsync" "qemu-img" "qemu-nbd" "xz")
    
    for CMD in "${REQUIRED_COMMANDS[@]}"; do
        check_command "$CMD"
    done
    
    if ! modinfo nbd &>/dev/null; then
        error_exit "The kernel module nbd is not available. Install it with 'apt install nbd-client'"
    fi
    
    log "SUCCESS" "All dependencies are satisfied"
}

function main {
    # Root privileges are required for operations like mounting filesystems and using chroot
    if [ "$EUID" -ne 0 ]; then
        error_exit "This script must be run as root"
    fi
    
    EDITIONS=("${DEFAULT_EDITIONS[@]}")
    ARCHS=("${DEFAULT_ARCHS[@]}")
    FORMATS=("${DEFAULT_IMAGE_FORMATS[@]}")
    IMAGE_SIZE="$DEFAULT_IMAGE_SIZE"
    
    while [[ $# -gt 0 ]]; do
        case $1 in
            -h|--help)
                show_usage
                ;;
            -v|--version)
                VERSION="$2"
                shift 2
                ;;
            -c|--codename)
                CODENAME="$2"
                shift 2
                ;;
            # Split comma-separated editions into an array.    
            -e|--editions)
                IFS=',' read -ra EDITIONS <<< "$2"
                shift 2
                ;;
            -a|--archs)
                IFS=',' read -ra ARCHS <<< "$2"
                shift 2
                ;;
            -f|--formats)
                IFS=',' read -ra FORMATS <<< "$2"
                shift 2
                ;;
            -s|--size)
                IMAGE_SIZE="$2"
                shift 2
                ;;
            -o|--output)
                OUTPUT_DIR="$2"
                shift 2
                ;;
            -n|--no-compress)
                COMPRESS=false
                shift
                ;;
            *)
                error_exit "Unrecognized option: $1"
                ;;
        esac
    done
    
    for EDITION in "${EDITIONS[@]}"; do
        if [[ ! " ${DEFAULT_EDITIONS[*]} " =~ " ${EDITION} " ]]; then
            error_exit "Invalid edition: $EDITION"
        fi
    done
    
    for ARCH in "${ARCHS[@]}"; do
        if [[ ! " ${DEFAULT_ARCHS[*]} " =~ " ${ARCH} " ]]; then
            error_exit "Invalid architecture: $ARCH"
        fi
    done
    
    for FORMAT in "${FORMATS[@]}"; do
        if [[ ! " ${IMAGE_FORMATS[*]} " =~ " ${FORMAT} " ]]; then
            error_exit "Invalid image format: $FORMAT"
        fi
    done
    
    check_dependencies
    
    mkdir -p "$OUTPUT_DIR"
    
    for ARCH in "${ARCHS[@]}"; do
        for EDITION in "${EDITIONS[@]}"; do
            log "INFO" "Starting build for $EDITION-$ARCH"
            
            case $EDITION in
                "home")
                    bootstrap "$ARCH" "$EDITION" "$CODENAME"
                    configure_home "$ARCH" "$EDITION"
                    ;;
                "security")
                    # If the Home Edition doesn't exist, create it first
                    if [ ! -d "home-$ARCH" ]; then
                        bootstrap "$ARCH" "home" "$CODENAME"
                        configure_home "$ARCH" "home"
                    fi
                    bootstrap "$ARCH" "$EDITION" "$CODENAME"
                    configure_security "$ARCH" "$EDITION"
                    ;;
                *)
                    error_exit "Unsupported edition: $EDITION"
                    ;;
            esac
            
            for FORMAT in "${FORMATS[@]}"; do
                create_image "$ARCH" "$EDITION" "$VERSION" "$FORMAT" "$IMAGE_SIZE"
            done
            
            if [ "$COMPRESS" = true ]; then
                compress_images "$ARCH" "$EDITION" "$VERSION"
            fi
            
            log "SUCCESS" "Build for $EDITION-$ARCH completed"
        done
    done
    
    log "SUCCESS" "All images have been built successfully!"
}

main "$@"